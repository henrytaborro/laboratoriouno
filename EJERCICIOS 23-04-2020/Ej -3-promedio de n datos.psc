Proceso calcular_promedio_N_materias
	definir n, i, dato, acum  Como Entero;
	definir promedio Como Real;
	escribir " ingrese los numero de datos";
	leer n;
	acum<-0;
	para i<-1 hasta n Hacer
		escribir " Ingrese dato ", i, ":";
		leer dato;
		acum<- acum + dato;
	FinPara
	promedio<- acum/n;
	escribir " el promedio de los datos ingresado es =", promedio;
FinProceso

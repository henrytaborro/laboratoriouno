Proceso riesgo_efermedad_coronaria
	definir IMC , altura , peso Como Real;
	definir edad Como Entero;
	definir riesgo como Caracter;
	Escribir " Ingrese la edad del paciente: ";
	Leer edad;
	Escribir " Ingrese el peso del paciente en K g : ";
	Leer peso;
	Escribir " Ingrese altura en metros : ";
	Leer altura;
	IMC <- (peso) / (altura ^ 2);
	si edad <45 Entonces
		si imc <22 Entonces
			riesgo <- " bajo ";
		SiNo
			riesgo <- " medio ";
		FinSi
	SiNo
		si imc <22 Entonces
			riesgo <- " medio ";
		SiNo
			riesgo <- " alto ";
		FinSi
	FinSi
	Escribir " riesgo de enfermedad coronaria :  ", riesgo;
	
FinProceso

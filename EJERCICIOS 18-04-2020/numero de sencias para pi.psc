Proceso numerosdeterminosdepi
	definir s Como Real;
	definir n como Entero;
	definir i Como Entero;
	s <- 0;
	Escribir"Ingrese la cantidad de terminos";
	Leer n;
	Para i<-1 hasta n Hacer
		Si i MOD 2=0 Entonces
			s <- s - (4 / ((i*2) -1));
		SiNo
			s <- s + (4/((i*2) -1));
			
		FinSi
	FinPara
	Escribir "El numero PI =", s;
FinProceso
